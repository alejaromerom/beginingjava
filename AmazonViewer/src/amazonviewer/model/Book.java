/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amazonviewer.model;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author sigmotoa
 */
public class Book extends Publication implements IViewable {

    private int id;
    private String isbn;

    private int timeReaded;

    private boolean readed;

    public Book(String title, Date editionDate, String editorial) {
        super(title, editionDate, editorial);
    }

    public int getId() {
        return id;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public int getTimeReaded() {
        return timeReaded;
    }

    public void setTimeReaded(int timeReaded) {
        this.timeReaded = timeReaded;
    }

    public String isReaded() {
         String read=null;
        if(readed==true)
        {
            read="Si";
        }
        else
        {
            read ="No";
        }
        return read;}

    public void setReaded(boolean readed) {
        this.readed = readed;
    }

    @Override
    public String toString() {
        return "::BOOK::"
                + "\n Title: " + getTitle()
                + "\n Edition: " + getEditionDate()
                + "\n Editorial: " + getEditorial();

    }

    @Override
    public Date stratToSee(Date dateI) {

        return dateI;
    }

    @Override
    public void stopToSee(Date dateI, Date dateF) {
        if (dateF.getSeconds() > dateI.getSeconds()) {
            setTimeReaded(dateF.getSeconds() - dateI.getSeconds());

        } else {
            setTimeReaded(0);

        }

    }

    public static ArrayList<Book> makeBooksList() {
        ArrayList<Book> books = new ArrayList();

        for (int i = 1; i < 6; i++) {
            books.add(new Book("Librote " + (i + 1), new Date(), "sigmotoa"));

        }

        return books;
    }

}
